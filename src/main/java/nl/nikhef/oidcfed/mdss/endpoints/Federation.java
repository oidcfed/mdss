package nl.nikhef.oidcfed.mdss.endpoints;

import lombok.SneakyThrows;
import nl.nikhef.oidcfed.mdss.data.EntityStatement;
import nl.nikhef.oidcfed.mdss.exceptions.UnsupportedParameterException;
import nl.nikhef.oidcfed.mdss.strategies.FederationStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("signing-service")
public class Federation {

    @Value("${oidcfed.whoami}")
    private String whoami;

    @Autowired
    private FederationStrategy strategy;

    @ResponseBody
    @GetMapping(path = "fetch", produces = "application/jose")
    public EntityStatement fetch(
            @RequestParam(name = "iss", required = false) String issuer,
            @RequestParam(name = "sub", required = false) String subject
    ) {
        if (issuer == null) {
            issuer = whoami;
        }

        return strategy.fetch(issuer, subject);
    }

    @SneakyThrows
    @ResponseBody
    @GetMapping(path = "list", produces = "application/json")
    public List<String> list(
            @RequestParam(name = "entity_type", required = false, defaultValue = "") String entityType,
            @RequestParam(name = "trust_marked", required = false) String trustMarked,
            @RequestParam(name = "trust_mark_id", required = false) String trustMarkId
    ) {
        if (trustMarked != null || trustMarkId != null) {
            throw new UnsupportedParameterException("Trust Mark related parameters are not supported.");
        }

        return strategy.listing(entityType);
    }
}
