package nl.nikhef.oidcfed.mdss.endpoints;

import lombok.SneakyThrows;
import nl.nikhef.oidcfed.mdss.data.EntityStatement;
import org.jose4j.jwk.JsonWebKeySet;
import org.jose4j.jwk.PublicJsonWebKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.URL;
import java.time.Instant;
import java.util.Collections;
import java.util.function.Supplier;

@RestController
@RequestMapping(".well-known/openid-federation")
public class FederationEntityConfiguration {

    @Value("${oidcfed.signature_lifetime}")
    private Integer signatureLifetime;

    @Value("${oidcfed.whoami}")
    private String whoami;

    @Autowired
    private Supplier<PublicJsonWebKey> jwkSupplier;

    @ResponseBody
    @SneakyThrows
    @GetMapping(produces = "application/jose")
    public EntityStatement federationConfiguration(HttpServletRequest request) {
        return EntityStatement.builder()
                .iss(whoami)
                .sub(whoami)
                .iat(Instant.now().getEpochSecond())
                .exp(Instant.now().getEpochSecond()+signatureLifetime)
                .metadata(Collections.singletonMap("federation_entity",
                        Collections.singletonMap("federation_api_endpoint",
                                new URL(request.getScheme(), request.getServerName(), request.getServerPort(), "/signing-service"))))
                .jwks(new JsonWebKeySet(jwkSupplier.get()))
                .build();
    }
}
