package nl.nikhef.oidcfed.mdss.endpoints;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import nl.nikhef.oidcfed.mdss.data.EntityStatement;
import nl.nikhef.oidcfed.mdss.data.EntityStatementDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@RestController
@RequestMapping("mdss-management")
public class Management {

    @Value("${oidcfed.import_folder}")
    private String importFolder;

    @Autowired
    private EntityStatementDB statementDB;

    @SneakyThrows
    @GetMapping("import-all")
    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    public void importAllStatements() {
        statementDB.deleteAll();

        try (Stream<Path> files = Files.walk(Paths.get(importFolder))) {
            statementDB.saveAll(files
                    .map(Path::toFile)
                    .filter(File::isFile)
                    .map(this::readEntityStatement)
                    .collect(Collectors.toList()));
        }
    }

    @SneakyThrows
    private EntityStatement readEntityStatement(File f) {
        return new ObjectMapper().readerFor(EntityStatement.class).readValue(f);
    }
}
