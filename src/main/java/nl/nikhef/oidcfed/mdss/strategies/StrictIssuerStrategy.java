package nl.nikhef.oidcfed.mdss.strategies;

import lombok.SneakyThrows;
import nl.nikhef.oidcfed.mdss.data.EntityStatement;
import nl.nikhef.oidcfed.mdss.exceptions.UnacceptableRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConditionalOnProperty(
        prefix = "oidcfed",
        name = "strict",
        havingValue = "true"
)
public class StrictIssuerStrategy extends PermissiveIssuerStrategy {

    @Value("${oidcfed.whoami}")
    private String whoami;

    @Override
    @SneakyThrows
    public EntityStatement fetch(String issuer, String subject) {
        if (!StringUtils.equalsIgnoreCase(issuer, whoami)) throw new UnacceptableRequest();

        return super.fetch(issuer, subject);
    }
}
