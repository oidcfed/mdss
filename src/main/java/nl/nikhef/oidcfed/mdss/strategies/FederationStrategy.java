package nl.nikhef.oidcfed.mdss.strategies;

import nl.nikhef.oidcfed.mdss.data.EntityStatement;

import java.util.List;

public interface FederationStrategy {
    EntityStatement fetch(String issuer, String subject);
    List<String> listing(String entityType);
}
