package nl.nikhef.oidcfed.mdss.strategies;

import lombok.SneakyThrows;
import nl.nikhef.oidcfed.mdss.data.EntityStatement;
import nl.nikhef.oidcfed.mdss.data.EntityStatementDB;
import nl.nikhef.oidcfed.mdss.exceptions.UnacceptableRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@ConditionalOnProperty(
        prefix = "oidcfed",
        name = "strict",
        havingValue = "false",
        matchIfMissing = true
)
public class PermissiveIssuerStrategy implements FederationStrategy {

    @Value("${oidcfed.signature_lifetime}")
    private Integer signatureLifetime;

    @Lazy
    @Autowired
    private EntityStatementDB statementDB;

    @Override
    @SneakyThrows
    public EntityStatement fetch(String issuer, String subject) {
        if (StringUtils.isBlank(subject)) {
            subject = issuer;
        }

        EntityStatement statement = statementDB.getByIssAndSub(issuer, subject);

        if (statement == null)
            throw new UnacceptableRequest(String.format("Not willing to sign for %s as %s", subject, issuer));

        statement.setIat(Instant.now().getEpochSecond());
        statement.setExp(Instant.now().getEpochSecond() + signatureLifetime);

        return statement;
    }

    @Override
    public List<String> listing(String entityType) {
        Stream<EntityStatement> esStream = statementDB.findAll().stream();

        if (!entityType.isEmpty()) {
            esStream = esStream
                    .filter(es -> es.getMetadata().containsKey(entityType));
        }

        return esStream
                .map(EntityStatement::getSub)
                .collect(Collectors.toList());
    }
}
