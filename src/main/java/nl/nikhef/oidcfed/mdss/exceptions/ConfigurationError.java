package nl.nikhef.oidcfed.mdss.exceptions;

public class ConfigurationError extends Error {
    public ConfigurationError(String message) {
        super(message);
    }
}
