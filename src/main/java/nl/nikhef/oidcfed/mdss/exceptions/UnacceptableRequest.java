package nl.nikhef.oidcfed.mdss.exceptions;

public class UnacceptableRequest extends GenericFederationError {
    public UnacceptableRequest() {
        super("unacceptable_request","This MDSS is not willing to make statements for issuers other than its own");
    }

    public UnacceptableRequest(String message) {
        super("unacceptable_request", message);
    }
}
