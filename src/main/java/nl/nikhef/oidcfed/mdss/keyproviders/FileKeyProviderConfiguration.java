package nl.nikhef.oidcfed.mdss.keyproviders;

import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.jws.JsonWebSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.File;
import java.nio.charset.Charset;
import java.util.function.Supplier;

@Configuration
@ConditionalOnProperty(
        prefix = "oidcfed",
        name = "keyprovider",
        havingValue = "file"
)
public class FileKeyProviderConfiguration {

    @Value("${oidcfed.keyconfig}")
    private String keyconfig;

    private PublicJsonWebKey jwk;

    @SneakyThrows
    @PostConstruct
    public void init() {
        this.jwk = PublicJsonWebKey.Factory.newPublicJwk(
                FileUtils.readFileToString(new File(keyconfig), Charset.defaultCharset()));
    }

    @Bean
    public Supplier<PublicJsonWebKey> jwkSupplier() {
        return this::copyJwk;
    }

    @SneakyThrows
    private PublicJsonWebKey copyJwk() {
        return PublicJsonWebKey.Factory.newPublicJwk(this.jwk.toParams(JsonWebKey.OutputControlLevel.PUBLIC_ONLY));
    }

    @Bean
    @SneakyThrows
    public Supplier<JsonWebSignature> provideJWS() {
        return () -> {
            JsonWebSignature jws = new JsonWebSignature();

            if (StringUtils.isNotBlank(this.jwk.getKeyId()))
                jws.setKeyIdHeaderValue(this.jwk.getKeyId());

            jws.setAlgorithmHeaderValue(this.jwk.getAlgorithm());
            jws.setKey(this.jwk.getPrivateKey());

            return jws;
        };
    }
}
