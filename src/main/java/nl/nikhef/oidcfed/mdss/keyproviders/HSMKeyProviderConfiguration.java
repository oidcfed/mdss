package nl.nikhef.oidcfed.mdss.keyproviders;

import lombok.SneakyThrows;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.jws.JsonWebSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sun.security.pkcs11.SunPKCS11;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.security.KeyStore;
import java.security.Security;
import java.util.function.Supplier;

@Configuration
@ConditionalOnProperty(
        prefix = "oidcfed",
        name = "keyprovider",
        havingValue = "hsm"
)
public class HSMKeyProviderConfiguration {

    @Value("${oidcfed.keyconfig}")
    private String keyconfig;

    @Value("${oidcfed.hsm.algorithm}")
    private String hsmAlgorithm;

    @Value("${oidcfed.hsm.userpin}")
    private String userpin;

    @Value("${oidcfed.hsm.keyid}")
    private String keyname;

    @Value("${oidcfed.hsm.keypin}")
    private String keypin;

    private PublicJsonWebKey jwk;

    private Key hsmKey;

    @SneakyThrows
    @PostConstruct
    public void init() {
        SunPKCS11 hsmProvider = new SunPKCS11(keyconfig);
        Security.addProvider(hsmProvider);

        KeyStore hsm = KeyStore.getInstance("PKCS11", hsmProvider);
        hsm.load(null, userpin.toCharArray());
        this.hsmKey = hsm.getKey(keyname, keypin.toCharArray());

        this.jwk = PublicJsonWebKey.Factory.newPublicJwk(hsm.getCertificate(keyname).getPublicKey());
    }

    @Bean
    public Supplier<PublicJsonWebKey> jwkSupplier() {
        return this::copyJwk;
    }

    @SneakyThrows
    private PublicJsonWebKey copyJwk() {
        return PublicJsonWebKey.Factory.newPublicJwk(this.jwk.getPublicKey());
    }

    @Bean
    @SneakyThrows
    public Supplier<JsonWebSignature> provideJWS() {
        return () -> {
            JsonWebSignature jws = new JsonWebSignature();

            jws.setAlgorithmHeaderValue(hsmAlgorithm);
            jws.setKey(hsmKey);

            return jws;
        };
    }
}
