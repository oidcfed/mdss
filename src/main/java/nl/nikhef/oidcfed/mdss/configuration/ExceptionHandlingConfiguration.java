package nl.nikhef.oidcfed.mdss.configuration;

import lombok.AllArgsConstructor;
import lombok.Data;
import nl.nikhef.oidcfed.mdss.exceptions.GenericFederationError;
import nl.nikhef.oidcfed.mdss.exceptions.UnacceptableRequest;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestController
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ExceptionHandlingConfiguration extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UnacceptableRequest.class)
    protected ResponseEntity<Object> handleUnacceptableReqeuest(UnacceptableRequest ex, WebRequest request) {
        GenericFederationErrorData errorDetails =
                new GenericFederationErrorData("unacceptable_request", ex.getMessage());

        return new ResponseEntity<>(errorDetails, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(GenericFederationError.class)
    protected ResponseEntity<Object> handleGenericFederationError(GenericFederationError ex, WebRequest request) {
        GenericFederationErrorData errorDetails =
                new GenericFederationErrorData(ex.getError(), ex.getError_decription());

        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleDefaultException(Exception ex, WebRequest request) {
        GenericFederationErrorData errorDetails =
                new GenericFederationErrorData(ex.getClass().getSimpleName(), ex.getMessage());

        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body,
                                                          HttpHeaders headers, HttpStatus status,
                                                          WebRequest request) {
        GenericFederationErrorData errorDetails =
                new GenericFederationErrorData(ex.getClass().getSimpleName(), ex.getMessage());
        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
                                                                          HttpHeaders headers, HttpStatus status,
                                                                          WebRequest request) {
        GenericFederationErrorData errorDetails =
                new GenericFederationErrorData("invalid_request", ex.getMessage());

        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @Data
    @AllArgsConstructor
    private static class GenericFederationErrorData {
        private String error;
        private String error_decription;
    }
}
