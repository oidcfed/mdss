package nl.nikhef.oidcfed.mdss.configuration.converters;

import lombok.SneakyThrows;
import org.bson.Document;
import org.jose4j.jwk.JsonWebKeySet;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class JWKSReader implements Converter<Document, JsonWebKeySet> {

    @Override
    @SneakyThrows
    public JsonWebKeySet convert(Document source) {
        return new JsonWebKeySet(source.toJson());
    }
}
