package nl.nikhef.oidcfed.mdss.configuration;

import nl.nikhef.oidcfed.mdss.configuration.converters.JWKSReader;
import nl.nikhef.oidcfed.mdss.configuration.converters.JWKSWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

import java.util.Arrays;

@Configuration
public class MongoConversionConfiguration {

    @Bean
    public MongoCustomConversions customConversions() {
        return new MongoCustomConversions(Arrays.asList(new JWKSReader(), new JWKSWriter()));
    }
}
