package nl.nikhef.oidcfed.mdss.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import nl.nikhef.oidcfed.mdss.data.EntityStatement;
import org.apache.commons.io.IOUtils;
import org.jose4j.jws.JsonWebSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.stereotype.Component;

import java.nio.charset.Charset;
import java.util.function.Supplier;

@Component
public class EntityStatementEncoder extends AbstractHttpMessageConverter<EntityStatement> {

    public EntityStatementEncoder() {
        super(new MediaType("application", "jose"));
    }

    @Autowired
    private Supplier<JsonWebSignature> jwsSupplier;

    @Override
    protected boolean supports(Class<?> clazz) {
        return clazz.equals(EntityStatement.class);
    }

    @Override
    @SneakyThrows
    protected void writeInternal(EntityStatement entityStatement, HttpOutputMessage outputMessage) {
        JsonWebSignature jws = jwsSupplier.get();

        jws.setPayload(new ObjectMapper().writer().writeValueAsString(entityStatement));

        outputMessage.getBody().write(jws.getCompactSerialization().getBytes());
    }

    @Override
    @SneakyThrows
    protected EntityStatement readInternal(Class clazz, HttpInputMessage inputMessage) {
        JsonWebSignature jws = new JsonWebSignature();

        jws.setCompactSerialization(IOUtils.toString(inputMessage.getBody(), Charset.defaultCharset()));

        return new ObjectMapper().readerFor(EntityStatement.class).readValue(jws.getPayload());
    }
}
