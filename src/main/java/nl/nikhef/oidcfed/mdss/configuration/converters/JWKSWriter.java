package nl.nikhef.oidcfed.mdss.configuration.converters;

import lombok.SneakyThrows;
import org.bson.Document;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.JsonWebKeySet;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class JWKSWriter implements Converter<JsonWebKeySet, Document> {

    @Override
    @SneakyThrows
    public Document convert(JsonWebKeySet source) {
        return Document.parse(source.toJson(JsonWebKey.OutputControlLevel.PUBLIC_ONLY));
    }
}
