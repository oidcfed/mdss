package nl.nikhef.oidcfed.mdss.configuration.converters;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.JsonWebKeySet;

import java.io.IOException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JSONConverters {
    public static class JWKSDeserializer extends StdDeserializer<JsonWebKeySet> {
        public JWKSDeserializer() {
            super(JsonWebKeySet.class);
        }

        protected JWKSDeserializer(Class<?> vc) {
            super(vc);
        }

        @Override
        @SneakyThrows
        public JsonWebKeySet deserialize(JsonParser p, DeserializationContext ctxt) {
            return new JsonWebKeySet(p.readValueAsTree().toString());
        }
    }

    public static class JWKSSerializer extends StdSerializer<JsonWebKeySet> {
        public JWKSSerializer() {
            super(JsonWebKeySet.class);
        }

        protected JWKSSerializer(Class<JsonWebKeySet> t) {
            super(t);
        }

        @Override
        public void serialize(JsonWebKeySet value, JsonGenerator gen, SerializerProvider provider) throws IOException {
            gen.writeRawValue(value.toJson(JsonWebKey.OutputControlLevel.PUBLIC_ONLY));
        }
    }
}
