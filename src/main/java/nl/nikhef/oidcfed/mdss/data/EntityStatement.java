package nl.nikhef.oidcfed.mdss.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import nl.nikhef.oidcfed.mdss.configuration.converters.JSONConverters;
import org.jose4j.jwk.JsonWebKeySet;

import java.util.List;
import java.util.Map;


@Data
@Builder
@NoArgsConstructor(force = true)
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class EntityStatement {
    @NonNull private String iss;
    @NonNull private String sub;
    private long iat;
    private long exp;
    private String aud;

    @JsonSerialize(using = JSONConverters.JWKSSerializer.class)
    @JsonDeserialize(using = JSONConverters.JWKSDeserializer.class)
    @NonNull private JsonWebKeySet jwks;

    private List<String> authorityHints;
    @NonNull private Map<String, Map<String, ?>> metadata;
    private Map<String, Map<String, ?>> metadataPolicy;
    private Map<String, ?> constraints;
    private List<String> crit;
    private List<String> policyLanguageCrit;
    private List<Map<String, ?>> trust_marks;
    private Map<String, List<?>> trust_marks_issuers;
    private String trust_anchor_id;
}
