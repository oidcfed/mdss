package nl.nikhef.oidcfed.mdss.data;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface EntityStatementDB extends MongoRepository<EntityStatement, String> {
    List<EntityStatement> findAll();
    EntityStatement getByIssAndSub(String iss, String sub);
    List<EntityStatement> getByIss(String iss);
    List<EntityStatement> getBySub(String sub);
}
