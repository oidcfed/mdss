package nl.nikhef.oidcfed.mdss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.ValueConstants;

@SpringBootApplication
public class MdssApplication {

	public static void main(String[] args) {
		SpringApplication.run(MdssApplication.class, args);
	}

}
